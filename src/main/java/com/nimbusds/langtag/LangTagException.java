package com.nimbusds.langtag;


/**
 * Language tag exception.
 */
public class LangTagException extends Exception {


	/**
	 * Creates a new language tag exception with the specified message.
	 *
	 * @param message The exception message.
	 */
	public LangTagException(final String message) {
	
		super(message);
	}
}
