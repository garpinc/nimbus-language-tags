/**
 * Language tags (RFC-5646) for Java.
 *
 * <p>Implementation of "Tags for Identifying Languages", 
 * <a href="http://tools.ietf.org/html/rfc5646">RFC-5646</a>.
 *
 * <p>Supports normal language tags. Special private language tags beginning 
 * with "x" and grandfathered tags beginning with "i" are not supported.
 *
 * <p>See {@link com.nimbusds.langtag.LangTag} for details.
 *
 * <p>This package has no dependencies.
 */
package com.nimbusds.langtag;
